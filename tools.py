import scipy.io as sio
import numpy as np
from PIL import Image
import tensorflow as tf

label_colours = [[128, 64, 128], [244, 35, 231], [69, 69, 69]
                # 0 = road, 1 = sidewalk, 2 = building
                ,[102, 102, 156], [190, 153, 153], [153, 153, 153]
                # 3 = wall, 4 = fence, 5 = pole
                ,[250, 170, 29], [219, 219, 0], [106, 142, 35]
                # 6 = traffic light, 7 = traffic sign, 8 = vegetation
                ,[152, 250, 152], [69, 129, 180], [219, 19, 60]
                # 9 = terrain, 10 = sky, 11 = person
                ,[255, 0, 0], [0, 0, 142], [0, 0, 69]
                # 12 = rider, 13 = car, 14 = truck
                ,[0, 60, 100], [0, 79, 100], [0, 0, 230]
                # 15 = bus, 16 = train, 17 = motocycle
                ,[119, 10, 32]]
                # 18 = bicycle

label_colours_sm_data = [[0, 0, 0], [230, 25, 75], [145, 30, 180], [170, 110, 40],
                 # 0 = Background, 1 = Pedestrian, 2 = Rider, 3 = Cyclist
                 [255,250,200], [60, 180, 75], [240, 50, 230], [170, 255, 195],
                 # 4 = Motorcyclist, 5 =Small Veh, 6 = Large Veh, 7 = School Bus,                  
                 [0, 0, 128], [204, 190, 0], [255, 255, 255], [255, 255, 255],
                 # 8 = Ambulance, # 9 = Fire Truck, 
                 [255, 255, 255], [255, 255, 255], [255, 255, 255], [255, 255, 255], 
                 [255, 255, 255], [255, 255, 255], [255, 255, 255], [255, 255, 255], 
                 [255, 255, 255], [255, 255, 255], [255, 255, 255], [255, 255, 255], 
                 [255, 255, 255], [0, 130, 200], [245, 130, 48], [255, 225, 25],
                 # 24 = Off Light, 25 = Legal Road, 26 = Emergency Road, 27 = Single Solid White
                 [250,190,190], [0, 128, 128], [128, 128, 128], [128, 0, 0],
                 # 28 = Single Solid Yellow, 29 = Doub Solid White, 30 = Double Solid Yellow, 31 = Single Dashed White 
                 [128, 128, 0], [230, 190, 255], [188, 228, 229], [70, 240, 240],
                 # 32 = Single Dashed Yellow, 33 = Dou Dash White, 34 = Doub Dash Yellow, 35 = Bike Lane, 
                 [210, 245, 60] ]
                 # 36 = Pedestrian Crossing

matfn = './utils/color150.mat'
def read_labelcolours(matfn):
    mat = sio.loadmat(matfn)
    color_table = mat['colors']
    shape = color_table.shape
    color_list = [tuple(color_table[i]) for i in range(shape[0])]

    return color_list

def decode_labels(mask, img_shape, num_classes):
    if num_classes == 150:
        color_table = read_labelcolours(matfn)
    elif num_classes == 19:
        color_table = label_colours
    elif num_classes == 37:
        color_table = label_colours_sm_data

    color_mat = tf.constant(color_table, dtype=tf.float32)
    onehot_output = tf.one_hot(mask, depth=num_classes)
    onehot_output = tf.reshape(onehot_output, (-1, num_classes))
    pred = tf.matmul(onehot_output, color_mat)
    pred = tf.reshape(pred, (1, img_shape[0], img_shape[1], 3))
    
    return pred

#def decode_labels(mask, img_shape, num_classes):
#    if num_classes == 150:
#        color_table = read_labelcolours(matfn)
#    else:
#        color_table = label_colours
#
#    color_mat = tf.constant(color_table, dtype=tf.float32)
#    onehot_output = tf.one_hot(mask, depth=num_classes)
#    onehot_output = tf.reshape(onehot_output, (-1, num_classes))
#    pred = tf.matmul(onehot_output, color_mat)
#    pred = tf.reshape(pred, (1, img_shape[0], img_shape[1], 3))
#    
#    return pred
#
def prepare_label(input_batch, new_size, num_classes, one_hot=True):
    with tf.name_scope('label_encode'):
        input_batch = tf.image.resize_nearest_neighbor(input_batch, new_size) # as labels are integer numbers, need to use NN interp.
        input_batch = tf.squeeze(input_batch, squeeze_dims=[3]) # reducing the channel dimension.
        if one_hot:
            input_batch = tf.one_hot(input_batch, depth=num_classes)
            
    return input_batch
