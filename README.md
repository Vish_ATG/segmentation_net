# segmentation_net

This repository is the current segmentation network (tensorflow only). Slightly modified to train/test ICNET on SSIC Data

This code borrows implementation from PSPNet-Tensorflow and Icnet-tensorflow.

Current Modifications:
1. Strips all tf.slim and keras features from corresponding references and uses native tensorflow.nn function calls
2. Modified Interp Layer functionality.
3. Adds method to train on ATG_SM_DATA. (21 segmentation classes currently)
4. Adds capability to visualize masks for ATG_SM_DATA

Requirements
Python 2.7
Tensorflow 1.7 or greater.
Access to ATG_SM_DATA