import tensorflow as tf
import argparse
import cv2
import sys
import numpy as np

IMG_MEAN = np.array((103.939,116.779,123.68), dtype=np.float32)

def check_input(img):
    ori_h, ori_w = img.get_shape().as_list()[1:3]
    if ori_h % 32 != 0 or ori_w % 32 != 0:
        new_h = (int(ori_h/32) + 1) * 32
        new_w = (int(ori_w/32) + 1) * 32
        shape = [new_h, new_w]
        img = tf.image.pad_to_bounding_box(img, 0, 0, new_h, new_w)
        print('Image shape cannot divided by 32, padding to ({0}, {1})'.format(new_h, new_w))
    else:
        shape = [ori_h, ori_w]
    return img.eval(), shape





def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the 
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it 
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="prefix")
    return graph

if __name__ == '__main__':
    # Let's allow the user to pass the filename as an argument
    parser = argparse.ArgumentParser()
    parser.add_argument("--frozen_model_filename", default="results/frozen_model.pb", type=str, help="Frozen model file to import")
    parser.add_argument("--image_path", default="", type=str, help="Provide the path to inference image")
    args = parser.parse_args()

    # We use our "load_graph" function
    graph = load_graph(args.frozen_model_filename)
    
    #print([n.name for n in tf.get_default_graph().as_graph_def().node] )
    # We can verify that we can access the list of operations in the graph
    for op in graph.get_operations()[10:20]:
        print(op.name)
        print (op.values())
        # prefix/Placeholder/inputs_placeholder
        # ...
        # prefix/Accuracy/predictions
        
    # We access the input and output nodes 
    x = graph.get_tensor_by_name('prefix/Placeholder:0')
#    x = graph.get_tensor_by_name('prefix/create_inputs/batch/n:0')
    y = graph.get_tensor_by_name('prefix/Reshape_1:0')

    with tf.Session(graph=graph) as sess:
        try: 
         img = cv2.imread(args.image_path)
         img=np.float32(img)
         #img = img[:, :, ::-1]
        except:
         sys.exit("There should be a valid image readable by opencv")
        img -= IMG_MEAN
        #img = tf.expand_dims(img, dim=0)
        #img, shape = check_input(img) 
        y_out = sess.run(y, feed_dict={x:np.array(img)})
        y_out_new = np.squeeze(y_out, axis=0)
        cv2.imwrite('./output.jpg',cv2.cvtColor(y_out_new,cv2.COLOR_RGB2BGR))
        print (y_out.shape, y_out_new.shape)
    





# We launch a Session
#    with tf.Session(graph=graph) as sess:
#        # Note: we don't nee to initialize/restore anything
#        # There is no Variables in this graph, only hardcoded constants 
#        y_out = sess.run(y, feed_dict={
#            x: [[3, 5, 7, 4, 5, 1, 1, 1, 1, 1]] # < 45
#        })
#        # I taught a neural net to recognise when a sum of numbers is bigger than 45
#        # it should return False in this case
#        print(y_out) # [[ False ]] Yay, it works!
