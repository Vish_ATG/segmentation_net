##seg_Visulaize_blending.py
## This program alpha blends a color image mask with source image
# __author__ = 'Vish@SSIC-ATG'

from __future__ import print_function
import cv2
from PIL import Image
import numpy as np
import os
import sys
import argparse

def create_alpha_blend(src_image, mask_image, alpha, gamma):

    blended_image = cv2.addWeighted(mask_image, alpha, src_image, 1-alpha, 0, src_image)

    return blended_image

def check_src_mask_common_files(src_clip_path, mask_clip_path):
    
    if os.path.isdir(src_clip_path) and os.path.isdir(mask_clip_path):
        image_files = [x for x in os.listdir(src_clip_path) if os.path.isfile(os.path.join(src_clip_path,x))]
        mask_files =  [x for x in os.listdir(mask_clip_path) if os.path.isfile(os.path.join(mask_clip_path,x))]
        if '.DS_Store' in image_files:
            image_files.remove('.DS_Store')
        if '.DS_Store' in mask_files:
            mask_files.remove('.DS_Store')

        image_files_timestamp = set([x.split('_')[0] for x in image_files])
        mask_files_timestamp = set([x.split('_')[0] for x in mask_files])
        common_files = set.intersection(image_files_timestamp, mask_files_timestamp)

        if len(common_files) != len(image_files_timestamp) or len(common_files) != len(mask_files_timestamp):
            sys.stdout.write("Either some images or masks are missing in the given clips \n")
            sys.stdout.write("Ignoring Frames with Missing Images or Masks \n")

        images_with_masks = [os.path.join(src_clip_path, x) for x in image_files for y in common_files if y in x]
        masks_with_images = [os.path.join(mask_clip_path, x) for x in mask_files for y in common_files if y in x]

        # print  (len(images_with_masks), len(masks_with_images))
        return images_with_masks, masks_with_images
    elif os.path.isdir(src_clip_path) and not os.path.isdir(mask_clip_path) or \
         os.path.isdir(mask_clip_path) and not os.path.isdir(src_clip_path):
        sys.exit("Both Source and Mask Clip Path must be same, either a directory or individual file")
    else:
        assert os.path.basename(src_clip_path) == os.path.basename(mask_clip_path)
        #print (src_clip_path.split()[-1])
        #print (os.path.basename(src_clip_path))
        image_files = [src_clip_path]
        mask_files = [mask_clip_path]
        return image_files, mask_files

#def check_src_mask_common_files(src_clip_path, mask_clip_path):
#
#    image_files = [x for x in os.listdir(src_clip_path) if os.path.isfile(os.path.join(src_clip_path,x))]
#    mask_files =  [x for x in os.listdir(mask_clip_path) if os.path.isfile(os.path.join(mask_clip_path,x))]
#    
#    if '.DS_Store' in image_files:
#        image_files.remove('.DS_Store')
#    if '.DS_Store' in mask_files:
#        mask_files.remove('.DS_Store')
#
#    image_files_timestamp = set([x.split('_')[0] for x in image_files])
#    mask_files_timestamp = set([x.split('_')[0] for x in mask_files])
#    common_files = set.intersection(image_files_timestamp, mask_files_timestamp)
#
#    if len(common_files) != len(image_files_timestamp) or len(common_files) != len(mask_files_timestamp):
#        sys.stdout.write("Either some images or masks are missing in the given clips \n")
#        sys.stdout.write("Ignoring Frames with Missing Images or Masks \n")
#
#    images_with_masks = [os.path.join(src_clip_path, x) for x in image_files for y in common_files if y in x]
#    masks_with_images = [os.path.join(mask_clip_path, x) for x in mask_files for y in common_files if y in x]
#
#    # print  (len(images_with_masks), len(masks_with_images))
#    return images_with_masks, masks_with_images

def create_clip_visualize_seg(src_clip_path, mask_clip_path, output_path, alpha, gamma, file_extension):

    image_files, mask_files = check_src_mask_common_files(src_clip_path, mask_clip_path)

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for image, mask in zip(sorted(image_files), sorted(mask_files)):
        image_orig = cv2.imread(image)
        image_mask = cv2.imread(mask)
        image_blended = cv2.addWeighted(image_mask, alpha, image_orig, 1 - alpha, gamma, image_orig)
        blended_name = os.path.basename(image)
        blended_name.replace('.jpg', file_extension)
        #print blended_name
        cv2.imwrite(os.path.join(output_path, blended_name), image_blended)
    print ("SUCCESS")

# This function is to read and parse the command line arguments
def get_arguments():
    parser = argparse.ArgumentParser(description="Blending Source Image with Color Masks")
    parser.add_argument("src_clip_path", type=str, default='',
                        help="Provide the input path to folder with JPG image source files")
    parser.add_argument("mask_clip_path", type=str, default='',
                        help="Provide the path to color masks corresponding to the clip provided in source clip")
    parser.add_argument("output_path", type=str, default='',
                        help="Provide the output path to store blended visualization images")
    parser.add_argument("--alpha",type=float, default=0.4,
                        help="Provide a float value between 0 to 1 for the" +\
                             "amount of information from source to be retained")
    parser.add_argument("--gamma",type=float, default=0,
                        help="Provide a float value between 0 to 1 for the" +\
                             "scalar added to each sum")
    parser.add_argument("--extension", type=str, default='jpg',
                        help="Provide the extension for the images to be saved.",
                        choices=['bmp', 'jpg', 'jpeg', 'png'])
    return parser.parse_args()

#check_src_mask_common_files('/Users/v.rajalingam/Desktop/output/sample_test/1524593463.461218/FLC_R/','/Users/v.rajalingam/Desktop/output/exp_6/exp_6_1524593463.461218_FLC_R_color/')
#alpha, gamma = 0.4, 0
#extension = '.jpg'

#src_clip_path = '/Users/v.rajalingam/Desktop/output/sample_test/1524593463.461218/FLC_R/'
#mask_clip_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_1524593463.461218_FLC_R_color/'
#out_path = '/Users/v.rajalingam/Desktop/output/exp_6/1524593463.461218_exp_6_blended_FLC_R/'
#

#src_clip_path = '/Users/v.rajalingam/Desktop/output/sample_test/1521678684.798149/'
#mask_clip_path ='/Users/v.rajalingam/Desktop/output/exp_6/exp_6_300000_steps_1521678684.798149_FLR_color/'
#out_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_300000_steps_1521678684.798149_FLR_color/'
#

#mask_clip_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_300000_Steps_1524593463.461218_FLC_R_color/'
#src_clip_path = '/Users/v.rajalingam/Desktop/output/sample_test/1524593463.461218/FLC_R/'
#out_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_300000_Steps_1524593463.461218_FLC_R_Blended/'

#src_clip_path = '/Users/v.rajalingam/Desktop/output/sample_test/1532099777.281411'
#mask_clip_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_color_1532099777.281411'
#out_path = '/Users/v.rajalingam/Desktop/output/exp_6/exp_6_blended_1532099777.281411'
def main():
    args = get_arguments()
    create_clip_visualize_seg(args.src_clip_path, args.mask_clip_path, args.output_path, args.alpha, args.gamma, 
    args.extension)

if __name__=='__main__':
    main()
