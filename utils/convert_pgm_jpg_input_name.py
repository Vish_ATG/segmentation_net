from __future__ import print_function 

## convert_pgm_jpg.py ##
## Property of Samsung Semiconductor Inc ##
## Date: 09/07/2018

__author__ = "Vish@SSIC-ATG"

import cv2
import numpy as np
from PIL import Image
import argparse
import os
import sys





### Function to process multiple directories and sub directories
def convertPGM_dirs(inputPath, outputPath, ext, jpg_root_path):
 
    validExtension = ['bmp', 'jpg', 'jpeg', 'png']
    if ext not in validExtension:
        print ("Given Extesnion not a valid one, jpg will be used")
        ext = 'jpg'

    if not os.path.exists (inputPath):
        sys.exit("The current input folder doesnot exist, please verify the path")

    elif os.path.isdir(inputPath):

        for dir_path, dirs, file_names in os.walk(inputPath):
            files = [x for x in file_names if x.endswith('.pgm')]
            dirname=os.path.basename(dir_path)

            if jpg_root_path and os.path.exists(os.path.join(jpg_root_path, dirname)):
#                if '_FLR' or '_FCR' in os.listdir(os.path.join(jpg_root_path, dirname))[0]:
                if os.listdir(os.path.join(jpg_root_path, dirname))[5].endswith('_FLR.jpg') or \
                   os.listdir(os.path.join(jpg_root_path, dirname))[5].endswith('_FCR.jpg'):
                   
                    suffix = os.listdir(os.path.join(jpg_root_path, dirname))[0].split('_')[-1].rstrip('.jpg')
                    print (suffix)
                
                else:
                    suffix='' 

            if not os.path.exists (os.path.join(outputPath, dirname)):
                 os.makedirs(os.path.join(outputPath, dirname))

            for filename in files:
                im = cv2.imread(os.path.join(dir_path, filename), -1)
                im8 = im.astype(np.uint8) 
                im8_bgr = cv2.cvtColor(im8, cv2.COLOR_BAYER_BG2BGR)
                
                if suffix:
                    op_orig_file = filename.split('_')[0]
                    op_file = os.path.join(outputPath, dirname, op_orig_file+'_'+suffix+'.'+ext)
#                    op_orig_file = filename.split('_')[0]
                else:
                    op_file = os.path.join(outputPath, dirname, filename.strip('.pgm')+'_R.'+ext)

                cv2.imwrite(op_file, im8_bgr) 
                #cv2.imwrite(os.path.join(outputPath,dirname, filename.strip('.pgm')+'_R.'+ext), im8_bgr)

    elif os.path.basename(inputPath).endswith('.pgm'):
        im = cv2.imread(os.path.join(inputPath, filename), -1)
        im8 = im.astype(np.uint8) 
        im8_bgr = cv2.cvtColor(im8, cv2.COLOR_BAYER_BG2BGR)
        cv2.imwrite(os.path.join(outputPath+'_R.'+ext), im8_bgr)


### Function to process multiple directories and sub directories
#def convertPGM_dirs(inputPath, outputPath, ext):
#    validExtension = ['bmp', 'jpg', 'jpeg', 'png']
#    if ext not in validExtension:
#        print ("Given Extesnion not a valid one, jpg will be used")
#        ext = 'jpg'
#
#    if not os.path.exists (inputPath):
#        sys.exit("The current input folder doesnot exist, please verify the path")
#
#    elif os.path.isdir(inputPath):
#        for dir_path, dirs, file_names in os.walk(inputPath):
#            files = [x for x in file_names if x.endswith('.pgm')]
#            dirname=os.path.basename(dir_path)
#            if not os.path.exists (os.path.join(outputPath, dirname)):
#                 os.makedirs(os.path.join(outputPath, dirname))
#            for filename in files:
#                im = cv2.imread(os.path.join(dir_path, filename), -1)
#                im8 = im.astype(np.uint8) 
#                im8_bgr = cv2.cvtColor(im8, cv2.COLOR_BAYER_BG2BGR)
#                cv2.imwrite(os.path.join(outputPath,dirname, filename.strip('.pgm')+'_R.'+ext), im8_bgr)
#
#    elif os.path.basename(inputPath).endswith('.pgm'):
#        im = cv2.imread(os.path.join(inputPath, filename), -1)
#        im8 = im.astype(np.uint8) 
#        im8_bgr = cv2.cvtColor(im8, cv2.COLOR_BAYER_BG2BGR)
#        cv2.imwrite(os.path.join(outputPath+'_R.'+ext), im8_bgr)

# This function reads a given folders, converts PGM to RGB images and saves it in a format of given extension.
# Inputs
# 1. A folder with PGM files
# 2. A path to save output files, if does not exist creates one.
# 3. The extension of image to save the RGB images. 
def convertPGM(inputPath, outputPath, ext):
    validExtension = ['bmp', 'jpg', 'jpeg', 'png']
    if ext not in validExtension:
        print ("Given Extesnion not a valid one, jpg will be used")
        ext = 'jpg'
    if not os.path.exists (inputPath):
        sys.exit("The current input folder doesnot exist, please verify the path")
    else:
        files = [x for x in os.listdir(inputPath) if x.endswith('.pgm')]
        if not os.path.exists (outputPath):
            os.makedirs(outputPath)
        for filename in files:
            im = cv2.imread(os.path.join(inputPath, filename), -1)
            im8 = im.astype(np.uint8)
            im8_bgr = cv2.cvtColor(im8, cv2.COLOR_BAYER_BG2BGR)
            cv2.imwrite(os.path.join(outputPath, filename.strip('.pgm')+'_R.'+ext), im8_bgr)

# This function is to read and parse the command line arguments
def get_arguments():
    parser = argparse.ArgumentParser(description="PGM2JPG")
    parser.add_argument("input_path", type=str, default='',
                        help="Provide the input path to folder with PGM image files")
    parser.add_argument("output_path", type=str, default='./',
                        help="Provide the output path to folder to save RGB image files. Make sure you provide with sub_dir name")
    parser.add_argument("extension", type=str, default='jpg',
                        help="Provide the extension for the images to be saved.",
                        choices=['bmp', 'jpg', 'jpeg', 'png'])
    parser.add_argument("--jpg_root_path", type=str, default='')
    return parser.parse_args()
 
# A Main function that parse command line arguments and calls the convertPGM Utility
def main():
        
    args = get_arguments()
    convertPGM_dirs(args.input_path, args.output_path, args.extension, args.jpg_root_path)

   # if args.jpg_root_path:
   #     convertPGM_dirs_jpg(args.input_path, args.output_path, args.extension, args.jpg_root_path)
   # else:
   #     print ("A root path for JPG's folder is not provided. The new jpg filenames won't match original.")
   #     convertPGM_dirs(args.input_path, args.output_path, args.extension)

if __name__ == "__main__":
    main()
